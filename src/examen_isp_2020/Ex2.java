package examen_isp_2020;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

    public class Ex2 {
        static JTextField field1, field2;
        static JButton button;
        static JTextArea textArea;

        public static void main(String[] args) {
            JFrame frame = new JFrame("Ex2");
            frame.getContentPane().setLayout(new FlowLayout());
            field1 = new JTextField("Text field 1",10);
            field1.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    field1.setText("");
                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
            field2 = new JTextField("Text field 2",10);
            field2.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    field2.setText("");
                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
            button = new JButton("Calc suma");
            textArea = new JTextArea("Rezultatul operatiei");
            button.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    try{
                        int suma = Integer.parseInt(field1.getText()) + Integer.parseInt(field2.getText());
                        textArea.setText(String.valueOf(suma));
                    }
                    catch(Exception ex){
                        textArea.setText("input invalid");
                        ex.printStackTrace();
                    }
                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
            frame.getContentPane().add(field1);
            frame.getContentPane().add(field2);
            frame.getContentPane().add(button);
            frame.getContentPane().add(textArea);

            frame.setPreferredSize(new Dimension(200,200));
            frame.pack();
            frame.setVisible(true);
        }
    }

